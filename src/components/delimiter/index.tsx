import * as React from 'react';
import {Text, View} from 'native-base'
import {StyleSheet} from "react-native";

export interface Props {
    text?: string;
    count?: number;
    textSize?: number;
    textColor?: string;
    lineColor?: string;
}

export default class TextDelimiter extends React.Component<Props> {
    render(): React.ReactNode {
        return (
            <View style={{
                flexDirection: 'row',
                alignContent: 'center',
                alignItems: 'center'
            }}>
                <Text style={{
                    fontSize: this.props.textSize ? this.props.textSize : 14,
                    color: this.props.textColor ? this.props.textColor : '#adadad',
                    marginStart: 8,
                    marginEnd: 8
                }}>
                    {this.props.text}
                </Text>
                <View
                    style={{
                        flex: 1,
                        borderBottomColor: this.props.lineColor ? this.props.lineColor : '#3f3f3f',
                        borderBottomWidth: StyleSheet.hairlineWidth,
                    }}/>
                {
                    this.props.count ?
                        <Text style={{
                            fontSize: this.props.textSize ? this.props.textSize : 14,
                            color: this.props.textColor ? this.props.textColor : '#adadad',
                            marginStart: 8,
                            marginEnd: 8
                        }}>
                            {this.props.count}
                        </Text>
                        : null
                }
            </View>
        );
    }
}
