import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  tabBar: {
    backgroundColor: '#00BCD4'
  },
  tabBarIndicator: {
    backgroundColor: '#fff'
  },
  label: {
    color: '#fff',
    fontSize: 12,
    fontWeight: 'bold'
  }
})
