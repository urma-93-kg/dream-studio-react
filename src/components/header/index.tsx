import { Constants } from 'expo';
import { Body, Button, Icon, Left, Header as NBHeader, Title } from 'native-base';
import * as React from 'react';
import { NavigationScreenProps } from 'react-navigation';

export interface Props extends NavigationScreenProps {
  title: string;
  left?: React.ReactNode;
  body?: React.ReactNode;
  right?: React.ReactNode;
  icon?: string;
}

export interface State {
  selected: string;
}

export default class Header extends React.PureComponent<Props, State> {

  protected left = (
    <Left>
      <Button transparent>
        <Icon
          active
          name={this.props.icon || 'md-arrow-back'}
          onPress={() => this.props.navigation.goBack()}
        />
      </Button>
    </Left>
  );

  protected body = (
    <Body style={{flex: 3}}>
      <Title>{this.props.title}</Title>
    </Body>
  );

  protected right = null;

  render(): React.ReactNode {
    const { left, body, right } = this.props;

    return <NBHeader style={{marginTop: Constants.statusBarHeight}}>
      {left || this.left}

      {body || this.body}

      {right || this.right}



      {/*{right && <Right>*/}
          {/*<Picker2/>*/}
          {/*/!*<View style={{borderWidth: 0}}>*!/*/}
          {/*/!*<Picker*!/*/}
          {/*/!*mode="dropdown"*!/*/}
          {/*/!*style={{ width: 30}}*!/*/}
          {/*/!*selectedValue={this.state.selected}*!/*/}
          {/*/!*onValueChange={this.onValueChange.bind(this)}*!/*/}
          {/*/!*>*!/*/}
          {/*/!*<Picker.Item label="Wallet" value="key0" />*!/*/}
          {/*/!*<Picker.Item label="ATM Card" value="key1" />*!/*/}
          {/*/!*<Picker.Item label="Debit Card" value="key2" />*!/*/}
          {/*/!*<Picker.Item label="Credit Card" value="key3" />*!/*/}
          {/*/!*<Picker.Item label="Net Banking" value="key4" />*!/*/}
          {/*/!*</Picker>*!/*/}
          {/*/!*</View>*!/*/}
        {/*</Right>*/}
      {/*}*/}

    </NBHeader>
  }
}

// class Picker2 extends React.Component<{}, {itemValue: any}> {
//
//   readonly state: {itemValue: any} = {
//     itemValue: null
//   };
//
//   render(): React.ReactNode {
//     return (
//       <View style={{paddingRight: 10}}>
//         <Button iconLeft transparent light onPress={() => {
//           debugger;
//         }}>
//           <Icon name='more' />
//         </Button>
//         <Picker
//           selectedValue={this.state.itemValue}
//           onValueChange={(itemValue) => {debugger
//             console.log(arguments);
//             this.setState({itemValue});
//           }}
//           style={{ position: 'absolute', top: 0, width: 1000, height: 1000, opacity: 0 }}
//           mode="dropdown">
//           {/*<Picker.Item*/}
//             {/*key={'1'}*/}
//             {/*label={'1'}*/}
//             {/*value={1}*/}
//           {/*/>*/}
//           {/*<Picker.Item*/}
//             {/*key={'2'}*/}
//             {/*label={'2'}*/}
//             {/*value={2}*/}
//           {/*/>*/}
//         </Picker>
//       </View>
//     );
//   }
// }
