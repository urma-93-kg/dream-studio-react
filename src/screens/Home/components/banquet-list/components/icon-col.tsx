import { Button, Col, Icon, Input, Label, ListItem, Text } from 'native-base';
import * as React from 'react';
import { DatePickerAndroid, Image, TextInput, TimePickerAndroid, TouchableHighlight } from 'react-native';
import Dialog from 'react-native-dialog';
import { BanquetEntity } from '../../../../../types/banquet';
import { DateUtils } from '../../../../../utils/date';
import styles from '../styles';

interface Props {
    banquet: BanquetEntity;
    isClickable?: boolean;
    onPress: (id: string, guests: number, time: string) => void;
    deleteBanquet: (id: string) => void;
}

interface State {
    dialogVisibility: boolean;
    confirmDialogVisibility: boolean;
    guests: string;
    date: string;
    time: string;
}

export class BanquetIcon extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    render(): React.ReactNode {
        const {banquet} = this.props;

        if (banquet.place_obj.photo_url) {
            return (
                <Image
                    style={{width: 64, height: 64}}
                    resizeMode={'contain'}
                    source={{uri: banquet.place_obj.photo_url}}
                />
            );
        } else {
            return (
                <Text style={styles.banquetPlaceName}>
                    {banquet.place_obj.name}
                </Text>
            );
        }
    }
}

export default class IconCol extends React.Component<Props, State> {

  protected dateInput?: TextInput;
  protected timeInput?: TextInput;

  constructor(props: Props) {
    super(props);

    const { banquet } = this.props;
    const date = DateUtils.fromFullString(banquet.date);

    this.state = {
      dialogVisibility: false,
      confirmDialogVisibility: false,
      guests: String(banquet.guests_adult),
      date: `${date.getYearString()}-${date.getMonthStr()}-${date.getDateStr()}`,
      time: `${date.getHoursStr()}:${date.getMinutesStr()}`
    };
  }

  protected setGuestDialogVisibility = (dialogVisibility: boolean) => {
    this.setState({dialogVisibility});
  };

  protected showGuestDialog = () => {
    this.setGuestDialogVisibility(true);
  };

  protected hideGuestDialog = () => {
    this.setGuestDialogVisibility(false);
  };

  protected setConfirmDialogVisibility = (confirmDialogVisibility: boolean) => {
    this.setState({confirmDialogVisibility});
  };

  protected showConfirmDialog = () => {
    this.setConfirmDialogVisibility(true);
  };

  protected hideConfirmDialog = () => {
    this.setConfirmDialogVisibility(false);
  };

  protected getDateInput = (input: any) => {
    this.dateInput = input;
  };

  protected getTimeInput = (input: any) => {
    this.timeInput = input;
  };

  protected submit = () => {
    const { guests, date, time } = this.state;
    const { banquet } = this.props;

    if (!guests) {
      return;
    }

    this.setGuestDialogVisibility(false);
    this.props.onPress(String(banquet.id), +guests, `${date} ${time}:00`);
  };

    protected deleteBanquet = () => {
        debugger;
        const {deleteBanquet, banquet} = this.props;

        this.hideConfirmDialog();
        this.hideGuestDialog();
        deleteBanquet(String(banquet.id));
    };

  onPress = () => this.showGuestDialog();

  onDeletePress = () => this.showConfirmDialog();

  protected onTextChange = (text: string) => {
    const guests = parseInt(text, 10);

    this.setState({
      guests: isNaN(guests) ? '' : String(guests)
    });
  };

  protected onDateFocus = async () => {
    // @ts-ignore
    this.dateInput && this.dateInput._root.blur();

    const { banquet } = this.props;
    const date = DateUtils.fromFullString(banquet.date);

    const {
      action,
      year = date.getFullYear(),
      month = date.getMonth(),
      day
    } = await DatePickerAndroid.open({ date });

    if (action !== DatePickerAndroid.dismissedAction) {
      const date = DateUtils.fromDate(new Date(year, month, day));

      this.setState({
        date: date.getDateFormat()
      });
    }
  };

  protected onTimeFocus = async () => {
    // @ts-ignore
    this.timeInput && this.timeInput._root.blur();

    const { banquet } = this.props;
    const date = DateUtils.fromFullString(banquet.date);

    const {action, hour, minute} = await TimePickerAndroid.open({
      hour: date.getHours(),
      minute: date.getMinutes(),
      mode: 'clock',
    });

    if (action !== TimePickerAndroid.dismissedAction) {
      date.setHours(hour);
      date.setMinutes(minute);

      this.setState({
        time: date.getTimeFormat()
      });
    }
  };

    render(): React.ReactNode {
        const {banquet, isClickable} = this.props;
        const {dialogVisibility, confirmDialogVisibility, date, time, guests} = this.state;
        const banquetDate = DateUtils.fromFullString(banquet.date);

        return (
            <Col size={20} style={styles.iconCol}>
                <TouchableHighlight
                    onPress={isClickable ? this.onPress : ()=>{}}
                    style={{paddingTop: 10}}>
                    <BanquetIcon
                        onPress={()=>{}}
                        deleteBanquet={()=>{}}
                        banquet={banquet}
                    />
                    {/*
              <Image
              style={{width: 64, height: 64}}
              resizeMode={'contain'}
              source={banquet.place_obj.photo_url
                ? {uri: banquet.place_obj.photo_url}
                : require('../../../../../../assets/icon.png')}
            />*/}
                </TouchableHighlight>

          <Text>{banquet.guests_adult}</Text>
          <Text>
            {`${banquetDate.getHoursStr()}:${banquetDate.getMinutesStr()}`}
          </Text>

                <Dialog.Container visible={dialogVisibility} style={{flex: 1}}>
                    <Button icon transparent style={{position: 'absolute', top: 16, right: 16}}
                            onPress={this.onDeletePress}>
                        <Icon name='trash'/>
                    </Button>
                    <Dialog.Title>
                        Редактирование
                    </Dialog.Title>

            <ListItem noBorder noIndent style={{flex: 1, flexGrow: 0, flexDirection: 'row', margin: 10}}>
              <Label style={{flex: 1}}>Кол-во</Label>
              <Input
                style={{flex: 1, textAlign: 'center'}}
                placeholder={'Кол-во готей'}
                keyboardType={'numeric'}
                onChangeText={this.onTextChange}
                value={guests || ''}
              />
            </ListItem>

            <ListItem noBorder noIndent style={{flex: 1, flexGrow: 0, flexDirection: 'row', margin: 10}}>
              <Label style={{flex: 1}}>Дата</Label>
              <Input
                style={{flex: 1, textAlign: 'center'}}
                placeholder={'Время'}
                ref={this.getDateInput}
                onFocus={this.onDateFocus}
                value={date}
              />
            </ListItem>

                    <ListItem noBorder noIndent
                              style={{flex: 1, flexGrow: 0, flexDirection: 'row', margin: 10, marginBottom: 20}}>
                        <Label style={{flex: 1}}>Время</Label>
                        <Input
                            style={{flex: 1, textAlign: 'center'}}
                            placeholder={'Время'}
                            ref={this.getTimeInput}
                            onFocus={this.onTimeFocus}
                            value={time}
                        />
                    </ListItem>

            <Dialog.Button label={'Отмена'} onPress={this.hideGuestDialog}  />
            <Dialog.Button label={'Сохранить'} onPress={this.submit} />
          </Dialog.Container>

          <Dialog.Container visible={confirmDialogVisibility}>
            <Dialog.Title>Вы уверены?</Dialog.Title>
            <Dialog.Button label={'Нет'} onPress={this.hideConfirmDialog}  />
            <Dialog.Button label={'Да'} onPress={this.deleteBanquet} />
          </Dialog.Container>
        </Col>
      );
  }

}
