import React, {Component} from 'react';
import {Content, Container, ListItem, View} from "native-base";
import {FlatList, Text} from "react-native";
import Header from "../../components/header";
import {NavigationScreenProps} from "react-navigation";
import styles from './styles';
import {NavigateTo} from "../../app/app";

export interface Props extends NavigationScreenProps {

}

export interface State {

}

export interface ListEntity {
    title: string;
    navigateTo: NavigateTo,
    isPush: boolean
}

export default class Settings extends Component<Props, State> {

    listData: ListEntity[] = [
        {
            title: 'Изменить пароль',
            navigateTo: NavigateTo.CHANGE_PASSWORD,
            isPush: true
        },
    ];

    onItemPress = (item: ListEntity) => {
        if (item.isPush)
            this.props.navigation.push(item.navigateTo);
        else
            this.props.navigation.replace(item.navigateTo);
    };

    renderItem = ({item}) => {
        return (
            <ListItem onPress={() => this.onItemPress(item)}>
                <View>
                    <Text style={styles.listItem}>{item.title}</Text>
                </View>
            </ListItem>
        );
    };

    render(): React.ReactNode {
        return (
            <Container>
                <Header title={'Настройки'}
                        navigation={this.props.navigation}
                />
                <Content>
                    <FlatList data={this.listData}
                              renderItem={this.renderItem}
                              keyExtractor={item => item.title}
                    />
                </Content>
            </Container>
        );
    }

}