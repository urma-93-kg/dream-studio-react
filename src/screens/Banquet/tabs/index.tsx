import * as React from 'react';
import {BanquetEntity, BanquetStaffList} from '../../../types/banquet';
import {StaffEntity, StaffEntityList} from '../../../types/staff';
import List from './list';
import {View} from 'native-base'
import {FlatList} from "react-native";

export interface Props {
    type: number;
    staffList: StaffEntityList;
    banquet: BanquetEntity;
    banquetStaffList: BanquetStaffList;
    handleCheckBox: Function;
    filter: string;
}

interface StaffListData {
    data: StaffEntityList;
    title: string;
}

export default class BanquetTab extends React.Component<Props> {

    protected isChecked(staff: StaffEntity): boolean {
        return !!this.props.banquet.staff_occ.find(st => {
            return st.staff.id == staff.id && this.props.type === st.type;
        });
    }

    protected getName(): string {
        return this.getNameByType(this.props.type);
    }

    protected getTrueType(): number {
        return this.getTrueTypeByType(this.props.type);
    }

    private getTrueTypeByType(type: number): number {
        switch (type) {
            case 1:
                return 5;
            case 2:
                return 3;
            case 3:
                return 4;
            case 4:
                return 2;
            case 5:
                return 5;
            default:
                return 5;
        }
    }

    protected getNameByType(type: number): string {
        switch (type) {
            case 1:
                return 'свободные кураторы';
            case 2:
                return 'свободные фотографы';
            case 3:
                return 'свободные IT';
            case 4:
                return 'свободные раздача';
            default:
                return 'другие';
        }
    }

    private nextStaffSwitcher(staffList: StaffEntityList, n: number, order: number[]): StaffEntityList {
        switch (n) {
            case 1:
                return Object.values(staffList).filter(staff => staff.type == this.getTrueTypeByType(order[0]));
            case 2:
                return Object.values(staffList).filter(staff => staff.type == this.getTrueTypeByType(order[1]));
            case 3:
                return Object.values(staffList).filter(staff => staff.type == this.getTrueTypeByType(order[2]));
        }
        return [];
    }

    private nextTitleSwitcher(n: number, order: number[]): string {
        switch (n) {
            case 1:
                return this.getNameByType(order[0]);
            case 2:
                return this.getNameByType(order[1]);
            case 3:
                return this.getNameByType(order[2]);
        }
        return '';
    }

    /**
     * Returns list of Staffs on search in filter
     */
    private getNextStaffListFor(staffList: StaffEntityList, n: number): StaffListData {
        const {type} = this.props;
        switch (type) {
            case 1:
                return {
                    data: this.nextStaffSwitcher(staffList, n, [3, 4, 2]),
                    title: this.nextTitleSwitcher(n, [3, 4, 2])
                };
            case 2:
                return {
                    data: this.nextStaffSwitcher(staffList, n, [3, 4, 1]),
                    title: this.nextTitleSwitcher(n, [3, 4, 1])
                };
            case 3:
                return {
                    data: this.nextStaffSwitcher(staffList, n, [1, 4, 2]),
                    title: this.nextTitleSwitcher(n, [1, 4, 2])
                };
            case 4:
                return {
                    data: this.nextStaffSwitcher(staffList, n, [3, 1, 2]),
                    title: this.nextTitleSwitcher(n, [3, 1, 2])
                };
        }
        return {data: [], title: ''};
    }

    protected keyExtractor = ({title} : StaffListData) =>{
        return title;
    };

    render()
        :
        React.ReactNode {
        let {staffList, filter, banquet, type, handleCheckBox} = this.props;
        let staffChecked;
        let staffListByType;
        let staffOftenCoordinated;

        staffChecked = staffList.filter(staff => this.isChecked(staff));

        if (filter.length) {
            staffList = Object.values(staffList).filter(staff => staff.name.includes(filter));
        }

        staffList = staffList.filter(staff => !this.isChecked(staff));
        staffListByType = staffList.filter(staff => staff.type === this.getTrueType());
        staffOftenCoordinated = staffList.filter(staff => staff.is_often_coordinated && staff.type === this.getTrueType());

        // 1 - Администратор
        // 2 - Раздача
        // 3 - Фотограф
        // 4 - IT
        // 5 - Куратор


        let data = [];

        if (filter.length) {
            data = [
                {data: staffChecked, title: 'скоординированные'},
                {data: staffOftenCoordinated, title: 'часто координируемые'},
                {data: staffListByType, title: this.getName()},
                this.getNextStaffListFor(staffList, 1),
                this.getNextStaffListFor(staffList, 2),
                this.getNextStaffListFor(staffList, 3),
            ];
        } else {
            data = [
                {data: staffChecked, title: 'скоординированные'},
                {data: staffOftenCoordinated, title: 'часто координируемые'},
                {data: staffListByType, title: this.getName()}
            ];
        }

        return (
            <View>
                <FlatList data={data}
                          renderItem={
                              ({item}) => <List
                                  title={item.title}
                                  staffList={item.data}
                                  banquet={banquet}
                                  type={type}
                                  handleCheckBox={handleCheckBox}/>}
                          keyExtractor={this.keyExtractor}
                />
            </View>
        );
    }
}
