import { View } from 'native-base';
import * as React from 'react';
import { FlatList, ListRenderItemInfo } from 'react-native';
import { BanquetEntity } from '../../../types/banquet';
import { StaffEntity, StaffEntityList } from '../../../types/staff';
import Item from './item';
import TextDelimiter from "../../../components/delimiter";

interface Props {
    title?: string;
    staffList: StaffEntityList;
    banquet: BanquetEntity;
    type: number;
    handleCheckBox: Function;
}

interface State {
  offset: number;
  prevLength: number;
}

export default class List extends React.PureComponent<Props, State> {
  protected readonly limit = 20;

  readonly state: State = {
    offset: this.limit,
    prevLength: this.props.staffList.length
  };

  protected isChecked(staff: StaffEntity): boolean {
    return !!this.props.banquet.staff_occ.find(st => {
      return st.staff.id == staff.id && this.props.type === st.type;
    });
  }

  protected keyExtractor = (item: StaffEntity) => {
    return String(item.id);
  };

  protected renderItem = (item: ListRenderItemInfo<StaffEntity>) => {
    const {item: staff} = item;
    const {type, banquet, handleCheckBox} = this.props;
    const checked = this.isChecked(staff);

    return (
      <Item
        staff={staff}
        checked={checked}
        banquet={banquet}
        handleCheckBox={handleCheckBox}
        type={type}
        key={staff.id}
      />
    );
  };

  protected getItemLayout = (data: any, index: number) => {
    return {
      length: this.limit, offset: this.limit * index, index
    };
  };

  protected onEndReached = () => {
    const offset = this.state.offset + this.limit;

        if (offset <= this.props.staffList.length) {
            debugger
            this.setState({
                offset,
            });
        } else {
            debugger
            this.setState({
                offset: this.props.staffList.length,
            });
        }
    };

    protected getStaffList = (staffList: StaffEntityList) => {
        debugger
        return staffList.slice(0, this.state.offset);
    };

    getSnapshotBeforeUpdate(prevProps: Props, prevState: State) {
        if (this.props.staffList.length !== prevState.prevLength) {
            debugger
            this.setState({
                offset: this.limit,
                prevLength: this.props.staffList.length
            });
        }

    return null;
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any): void {
    // This method need for getSnapshotBeforeUpdate
  }

    render(): React.ReactNode {
        return (
            <View>
                {
                    this.props.staffList.length > 0
                        ? <TextDelimiter text={this.props.title} count={this.props.staffList.length}/>
                        : null
                }
                <FlatList<StaffEntity>
                    data={this.getStaffList(this.props.staffList)}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem}
                    onEndReached={this.onEndReached}
                    onEndReachedThreshold={0.5}
                />
            </View>
        );
    }
}
