import { Button, Input, Item, Text } from 'native-base';
import * as React from 'react';
import { TimePickerAndroid, ViewStyle } from 'react-native';
import { WrappedFieldProps } from 'redux-form';
import Localization from '../../../lib/localization';
import { DateUtils } from '../../../utils/date';
import styles from '../styles';

interface Props extends WrappedFieldProps {
  input: any;
}

export default class TimeField extends React.Component<Props> {

  protected setDateTime = async (): Promise<any> => {
    const date = DateUtils.fromDate(new Date());
    const { action,
      hour = date.getHours(),
      minute = date.getMinutes()
    } = await TimePickerAndroid.open({mode: 'clock'});

    if (action !== TimePickerAndroid.dismissedAction) {
      date.setHours(hour);
      date.setMinutes(minute);

      this.props.input.onChange(date.getTimeFormat());
    }
  };

  render(): React.ReactNode {
    const {input} = this.props;

    return (
      <Item style={styles.fieldWrapper as ViewStyle}>
        <Input {...input} disabled style={styles.field} />

        <Button iconRight transparent dark onPress={this.setDateTime}>
          <Text>{Localization.t('banquet.time')}</Text>
        </Button>
      </Item>
    );
  }
}
