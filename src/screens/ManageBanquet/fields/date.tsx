import { Button, Input, Item, Text } from 'native-base';
import * as React from 'react';
import { DatePickerAndroid, ViewStyle } from 'react-native';
import { WrappedFieldProps } from 'redux-form';
import Localization from '../../../lib/localization';
import { DateUtils } from '../../../utils/date';
import styles from '../styles';

interface Props extends WrappedFieldProps {
  input: any;
}

export default class DateField extends React.Component<Props> {

  protected setDateTime = async (): Promise<any> => {
    const date = new Date();
    const { action,
      year = date.getFullYear(),
      month = date.getMonth(),
      day = date.getDate()
    } = await DatePickerAndroid.open({
      date,
      mode: 'calendar'
    });

    if (action !== DatePickerAndroid.dismissedAction) {
      const date = DateUtils.fromDate(new Date(year, month, day));

      this.props.input.onChange(date.getDateFormat());
    }
  };

  render(): React.ReactNode {
    const {input} = this.props;

    return <Item style={styles.fieldWrapper as ViewStyle}>
      <Input {...input} disabled style={styles.field} />

      <Button iconRight transparent dark onPress={this.setDateTime}>
        <Text>{Localization.t('banquet.date')}</Text>
      </Button>
    </Item>
  }
}
