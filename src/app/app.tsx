import React from 'react';
import {Dimensions} from 'react-native';
import {createDrawerNavigator, createStackNavigator} from 'react-navigation';
import {Root as NativeBaseRoot} from 'native-base';
import AuthContainer from '../containers/Auth/index';
import Home from '../containers/Home/index';
import Banquet from '../containers/Banquet/index';
import ManageBanquetContainer from '../containers/ManageBanquet/index';
import ManageStaffContainer from '../containers/ManageStaff/index';
import DictionaryContainer from '../containers/Dictionary/index';
import Sidebar from '../containers/SidebarContainer/index';
import ScheduleContainer from '../containers/Schedule/index';
import Settings from "../screens/Settings/index";
import ChangePassword from "../containers/ChangePassword";

export enum NavigateTo {
    AUTH = 'auth',
    HOME = 'home',
    DRAWER = 'drawer',
    DRAWER_SIMPLE = 'drawer_simple',
    BANQUET = 'banquet',
    MANAGE_BANQUET = 'manageBanquet',
    MANAGE_STAFF = 'manageStaff',
    DICTIONARY = 'dictionary',
    SCHEDULE = 'schedule',
    SETTINGS = 'settings',
    CHANGE_PASSWORD = 'change_password',
}

const Drawer = createDrawerNavigator(
    {
        [NavigateTo.HOME]: Home,
        [NavigateTo.SCHEDULE]: ScheduleContainer,
        [NavigateTo.SETTINGS]: Settings
    },
    {
        drawerWidth: Dimensions.get('window').width - 50,
        drawerPosition: 'left',
        contentComponent: (props: any) => <Sidebar {...props} />,
        initialRouteName: NavigateTo.HOME,
    }
);

const DrawerSimple = createDrawerNavigator(
    {
        [NavigateTo.SCHEDULE]: ScheduleContainer,
        [NavigateTo.SETTINGS]: Settings
    },
    {
        drawerWidth: Dimensions.get('window').width - 50,
        drawerPosition: 'left',
        contentComponent: (props: any) => <Sidebar {...props} />,
        initialRouteName: NavigateTo.SCHEDULE,
    }
);

const App = createStackNavigator(
    {
        [NavigateTo.AUTH]: AuthContainer,
        [NavigateTo.DRAWER]: Drawer,
        [NavigateTo.DRAWER_SIMPLE]: DrawerSimple,
        [NavigateTo.BANQUET]: Banquet,
        [NavigateTo.MANAGE_BANQUET]: ManageBanquetContainer,
        [NavigateTo.MANAGE_STAFF]: ManageStaffContainer,
        [NavigateTo.DICTIONARY]: DictionaryContainer,
        [NavigateTo.SETTINGS]: Settings,
        [NavigateTo.CHANGE_PASSWORD]: ChangePassword,
    },
    {
        initialRouteName: NavigateTo.AUTH,
        headerMode: 'none'
    }
);

export interface Props {

}

export interface State {

}

class Root extends React.PureComponent<Props, State> {
    render(): React.ReactNode {
        return (
            <NativeBaseRoot>
                <App/>
            </NativeBaseRoot>
        );
    }

}

export default Root;
