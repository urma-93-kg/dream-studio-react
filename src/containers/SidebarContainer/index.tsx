import * as React from 'react';
import {NavigationScreenProps} from 'react-navigation';
import {connect} from 'react-redux';
import {createSelector} from 'reselect';
import {GlobalState} from '../../app/actions';
import Sidebar from '../../screens/Sidebar';
import {userInfo} from '../Auth/actions';
import {State as AuthState} from '../../types/auth';

export interface Props extends NavigationScreenProps {
    userInfo: Function;
    logOut: Function;
    user: any;
}

export interface State {
    isUser: boolean;
}

export class SidebarContainer extends React.Component<Props, State> {

    readonly state: State = {
        isUser: false
    };

    async componentDidMount(): Promise<any> {
        await this.lgetUser();
        this.setState({isUser: this.props.user})
    }

    protected async lgetUser(): Promise<any> {
        await this.props.userInfo();
    }

    render(): React.ReactNode {
        if (!this.state.isUser) {
            return null;
        }

        return <Sidebar
            navigation={this.props.navigation}
            user={this.props.user}
        />;
    }
}

const getUser = createSelector(
    (state: AuthState) => state.user,
    user => ({user})
);

const mapStateToProps = (state: GlobalState) => {
    return getUser(state.authState);
};

const mapDispatchToProps = (dispatch: any): any => {
    return {
        userInfo: () => dispatch(userInfo()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SidebarContainer);
