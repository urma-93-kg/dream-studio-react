import {
    Toast,
} from 'native-base';
import * as React from 'react';
import {NavigationScreenProps} from 'react-navigation';
import {connect} from 'react-redux';
import {createSelector} from 'reselect';
import {GlobalState} from '../../app/actions';
import {NavigateTo} from '../../app/app';
import AuthScreen from '../../screens/Auth';
import {SignInFormData} from '../../screens/Auth/SignIn/components/form';
import {logOut, signInAttempt, userInfo} from './actions';
import {State as AuthState} from '../../types/auth';

export interface Props extends NavigationScreenProps {
    token: string;
    user: any;
    loadUser: Function;
    signInAttempt: (formData: SignInFormData) => Promise<any>;
    logout: Function;
}

export interface State {
    payload: any;
    error: boolean
}

class AuthContainer extends React.Component<Props, State> {

    async componentDidMount(): Promise<any> {
        const {token, navigation} = this.props;

        if (navigation.getParam('logOut')) {
            this.logOut();
        } else if (token) {
            await this.lgetUser();
            if (this.props.user) {
                if (this.props.user.role == 1 || this.props.user.role == 2)
                    navigation.replace(NavigateTo.DRAWER);
                else
                    navigation.replace(NavigateTo.DRAWER_SIMPLE);
            }
        } else {
            this.logOut();
        }
    }

    protected async lgetUser(): Promise<any> {
        await this.props.loadUser();
    }

    protected logOut() {
        this.props.logout();
    }

    protected handleSubmit = (formData: SignInFormData) => {
        return this.props.signInAttempt(formData)
            .then(async () => {
                await this.lgetUser();
                if (this.props.user) {
                    if (this.props.user.role == 1 || this.props.user.role == 2)
                        this.props.navigation.replace(NavigateTo.DRAWER);
                    else
                        this.props.navigation.replace(NavigateTo.DRAWER_SIMPLE);
                }
            })
            .catch(() => {
                Toast.show({
                    text: 'Некорректный Email или пароль',
                    duration: 2000,
                    position: 'bottom',
                    textStyle: {textAlign: 'center'},
                });
            });
    };

    render(): React.ReactNode {
        return (
            <AuthScreen
                signInAttempt={this.handleSubmit}
                singUpAttempt={() => ''}
            />
        );
    }
}

const getToken = createSelector(
    (state: AuthState) => state.token,
    (token) => ({token})
);

const getUser = createSelector(
    (state: AuthState) => state.user,
    (user) => user
);

const mapStateToProps = (state: GlobalState) => {
    return {
        token: getToken(state.authState),
        user: getUser(state.authState),
    };
};

const mapDispatchToProps = (dispatch: any): any => {
    return {
        signInAttempt: (formData: SignInFormData) => dispatch(signInAttempt(formData)),
        loadUser: () => dispatch(userInfo()),
        logout: () => dispatch(logOut())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthContainer);
