import React, {Component} from "react";
import {Container, Content, Input} from "native-base";
import Header from "../../components/header";

import {NavigationScreenProps} from "react-navigation";

export interface Props extends NavigationScreenProps {
}

export default class ChangePassword extends Component<Props> {
    render(): React.ReactNode {
        return (
            <Container>
                <Header
                    title={'Изменить пароль'}
                    navigation={this.props.navigation}
                />
                <Content>
                    <Input placeholder='Действущий пароль'/>
                    <Input placeholder='Новый пароль'/>
                    <Input placeholder='Повторите новый пароль'/>
                </Content>
            </Container>
        );
    }
}