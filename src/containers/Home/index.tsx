import {Button, Fab, Icon, Left} from 'native-base';
import * as React from 'react';
import {NavigationScreenProps} from 'react-navigation';
import {connect} from 'react-redux';
import {createSelector} from 'reselect';
import {GlobalState} from '../../app/actions';
import {NavigateTo} from '../../app/app';
import HeaderSearch from '../../components/header-search';
import HomeScreen from '../../screens/Home';
import {BanquetList} from '../../screens/Home/components/banquet-list';
import {GroupBanquet, State as BanquetState} from '../../types/banquet';
import {deleteBanquet, loadBanquets, loadUser, updateBanquetTime} from './actions';
import {PropTypes} from "prop-types";
import {View as NView, ViewPropTypes} from 'react-native';
import {State as AuthState} from "../../types/auth";

const HiddenView = (props: any) => {
    const {children, hide, style} = props;
    if (hide) {
        return null;
    }
    return (
        <NView {...props} style={style}>
            {children}
        </NView>
    );
};

HiddenView.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
        PropTypes.number,
        PropTypes.arrayOf(PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
            PropTypes.element
        ])),
    ]).isRequired,
    style: ViewPropTypes.style,
    hide: PropTypes.bool,
};

interface Props extends NavigationScreenProps {
    userInfo: Function;
    loadBanquets: () => void;
    onUpdateTime: (id: string, guests: number, time: string) => Promise<any>;
    deleteBanquet: (id: string) => void;
    group: GroupBanquet;
    user: any;
}

interface State {
    filter: string;
    isFabHidden: boolean;
}

class HomeContainer extends React.Component<Props, State> {
    readonly state: State = {
        filter: '',
        isFabHidden: true,
    };

    protected timer: number = 0;

    componentDidMount(): void {
        const {loadBanquets} = this.props;

    loadBanquets();

    // @ts-ignore
    this.timer = setInterval(loadBanquets, 15000);
  }

  protected pushToManageBanquet = () => {
    this.props.navigation.push(NavigateTo.MANAGE_BANQUET, {
      onSave: this.onSaveBanquet
    });
  };

  protected onSaveBanquet = () => {
    this.props.loadBanquets();
  };

  protected leftNode = (
    <Left>
      <Button transparent>
        <Icon
          active
          name={'menu'}
          onPress={() => this.props.navigation.openDrawer()}
        />
      </Button>
    </Left>
  );

  protected onHeaderSetFilter = (filter: string) => {
    this.setState({filter});
  };

  protected headerNode = () => (
    <HeaderSearch
      title={'Координация'}
      navigation={this.props.navigation}
      left={this.leftNode}
      onSetFilter={this.onHeaderSetFilter}
      filter={this.state.filter}
    />
  );

    protected banquetListNode = () => (
        <BanquetList
            banquets={this.getBanquets()}
            navigation={this.props.navigation}
            user={this.props.user}
            onUpdateTime={this.props.onUpdateTime}
            deleteBanquet={this.props.deleteBanquet}
        />
    );

  protected getBanquets() {
    const { filter } = this.state;
    const { group } = this.props;

    if (!filter) {
      return group;
    }

    const result: GroupBanquet = {};

    Object.keys(group).forEach(date => {
      const list = (group[date] || []).filter(coordination => {
        if (coordination.place_obj.name.includes(filter)) {
          return true;
        }

        return coordination.staff_occ.some(staff => {
          return staff.staff.name.includes(filter)
        });
      });

      if (list.length) {
        result[date] = list;
      }
    });

    return result;
  }

  protected onUpdateTime = (id: string, guests: number, time: string) => {
    this.props.onUpdateTime(id, guests, time)
      .then(() => {
        this.props.loadBanquets();
      })
      .catch(() => {
        debugger
      })
  };

    render(): React.ReactNode {
        return (
            <HomeScreen
                header={this.headerNode()}
                banquetList={this.banquetListNode()}>
                <HiddenView
                    hide={this.props.user && this.props.user.role ? !(this.props.user.role == 1 || this.props.user.role == 2) : true}>
                    <Fab
                        position="bottomRight"
                        onPress={this.pushToManageBanquet}>
                        <Icon name="add"/>
                    </Fab>
                </HiddenView>
            </HomeScreen>
        );
    }

  componentWillUnmount(): void {
    clearInterval(this.timer);
  }
}

const getBanquets = createSelector(
    (state: BanquetState) => state.group,
    (group) => group
);

const getUser = createSelector(
    (state: AuthState) => state.user,
    user => user
);

const mapStateToProps = (state: GlobalState) => {
    return {
        group: getBanquets(state.banquetState),
        user: getUser(state.authState)
    };
};

const mapDispatchToProps = (dispatch: any): any => {
    return {
        userInfo: () => dispatch(loadUser()),
        loadBanquets: () => dispatch(loadBanquets()),
        onUpdateTime: (id: string, guests: number, time: string) => dispatch(updateBanquetTime(id, guests, time)),
        deleteBanquet: (id: string) => dispatch(deleteBanquet(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
