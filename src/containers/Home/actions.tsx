import { AnyAction, Dispatch } from 'redux';
import { BanquetDeleteSuccess, BanquetLoadSuccess, BanquetUpdateTimeSuccess } from '../../actions/banquet';
import API from '../../services';
import {ActionStandard, GetState} from '../../app/actions';
import {AuthUserInfoSuccess} from "../../actions/auth";

const prefix = 'banquet';
export const Actions = {
  ...ActionStandard,
  LOAD: `${prefix}-load`,
  SUCCESS: `${prefix}-load-success`,
  FAIL: `${prefix}-load-fail`
};

export const BANQUET_LOAD = 'banquet-load';
export const BANQUET_LOAD_SUCCESS = 'banquet-load-success';
export const BANQUET_LOAD_FAILURE = 'banquet-load-failure';

// export interface BanquetStaff {
//   id: number;
//   staff: {
//     id: number;
//     name: string;
//     type: number;
//   }
//   staff_id: number;
//   type: number;
// }
//
// export type BanquetStaffList = BanquetStaff[];
//
// export interface GroupStaffList {
//   [key: string]: BanquetStaffList;
// }
//
// export interface Banquet {
//   id: number;
//   date: string;
//   guests_adult: number;
//   place_obj: {
//     id: number;
//     name: string;
//     photo_url: string;
//   };
//   staff_occ: BanquetStaffList
// }
//
// export type BanquetList = Banquet[];
//
// export interface GroupBanquet {
//   [key: string]: BanquetList;
// }

export function loadBanquets() {
  return (dispatch: Dispatch, getState: GetState, api: API) => {
    return api.getBanquetService().load()
      .then(banquets => {
        dispatch(new BanquetLoadSuccess(banquets));
      })
      .catch((error: any) => {
        console.log('ERROR',error);
        dispatch(banquetLoadFailure());
      });
  }
}

export function loadUser() {
    return (dispatch: any, getState: GetState, api: API) => {
        return api.getAuthService().userInfo()
            .then((json: any) => {
                if (!json.hasOwnProperty('error')) {
                    dispatch(new AuthUserInfoSuccess(json));
                }
            })
    }
}

export function updateBanquetTime(id: string, guests: number, dateTime: string) {
  return (dispatch: Dispatch, getState: GetState, api: API) => {
    return api.getBanquetService().updateTime(id, guests, dateTime)
      .then(banquet => {
        return dispatch(new BanquetUpdateTimeSuccess(banquet));
      });
  };
}

export function deleteBanquet(id: string) {
  return (dispatch: Dispatch, getState: GetState, api: API) => {
    dispatch(new BanquetDeleteSuccess({id}));

    return api.getBanquetService().delete(id);
  };
}

export function banquetLoadSuccess(banquets: any): AnyAction {
  return {
    type: BANQUET_LOAD_SUCCESS,
    banquets
  }
}

export function banquetLoadFailure(): AnyAction {
  return {
    type: BANQUET_LOAD_FAILURE
  }
}
